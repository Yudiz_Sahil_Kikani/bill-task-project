var nBalance = 50000;                    //bank balance
const nBudget = 45000;                  //speding threshold
const nPriceofphone = 12000;           //phone price
const nAprice = 1500;                 //accessory price
const nTaxrate = 12;                 //tax rate

var nTotalAmount = 0;                  //final bill amount   

console.log("bankbalance = " + nBalance );


var nPricewithtax;                                                  //phone price with tax
function calculateTax(nPriceofphone){
    nPricewithtax = nPriceofphone * nTaxrate / 100 + nPriceofphone;
    console.log("phone price with tax :" + nPricewithtax);
}
console.log()
calculateTax(nPriceofphone);

var nPhone = 0;
var nAccesory = 0;
while((nTotalAmount + nPricewithtax < nBudget)){                                    //total phone amount
    nPhone ++;
    nBalance = nBalance - nPricewithtax;
    console.log("Remaining bankBalance after purchasing phone : "+ nBalance);
    
    if(nTotalAmount + nPricewithtax + nAprice < nBudget ){                          //accessory amount
        nAccesory ++;
        var totalAmount = nPricewithtax + nAprice;
        console.log("price of Accessory :" + nAprice);
        nBalance = nBalance - nAprice ;
        console.log("Remaining bankBalance after purchasing accesory : "+ nBalance);
        
    }
        
    nTotalAmount = nTotalAmount + totalAmount;
    console.log("Final bill Amount = " + nTotalAmount);                            //final bill calculation
    
}

document.getElementById('nTotalAmount').innerHTML=nTotalAmount
document.getElementById('nPhone').innerHTML=nPhone
document.getElementById('nAccesory').innerHTML=nAccesory
document.getElementById('nPricewithtax').innerHTML=nPricewithtax
document.getElementById('nAprice').innerHTML=nAprice

var nTotalPriceofphone = nPhone * nPricewithtax;
var nTotalPriceofaccessory = nAccesory * nAprice;

document.getElementById('nTotalPriceofphone').innerHTML=nTotalPriceofphone
document.getElementById('nTotalPriceofaccessory').innerHTML=nTotalPriceofaccessory

console.log("You have Purchased total " + nPhone +" "+ "phone");                   //no of phone you have purchase
console.log("You have Purchased total " + nAccesory +" "+ "Accesory");             //no of accesory you have purchase
console.log("You have  " + nAccesory + " Accesory" + " & " + nPhone + " phone");